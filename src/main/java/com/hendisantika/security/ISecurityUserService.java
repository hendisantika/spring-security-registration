package com.hendisantika.security;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-registration
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/17/17
 * Time: 05.40
 * To change this template use File | Settings | File Templates.
 */

public interface ISecurityUserService {

    String validatePasswordResetToken(long id, String token);

}
