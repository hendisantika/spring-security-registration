package com.hendisantika.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-registration
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/19/17
 * Time: 05.14
 * To change this template use File | Settings | File Templates.
 */

@Configuration
@EnableScheduling
@ComponentScan({"com.hendisantika.task"})
public class SpringTaskConfig {

}
