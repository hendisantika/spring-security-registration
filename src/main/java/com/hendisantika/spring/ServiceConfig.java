package com.hendisantika.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-registration
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/19/17
 * Time: 05.07
 * To change this template use File | Settings | File Templates.
 */


@Configuration
@ComponentScan({"com.hendisantika.service"})
public class ServiceConfig {
}
