package com.hendisantika.captcha;

import com.hendisantika.web.error.ReCaptchaInvalidException;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-security-registration
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/01/17
 * Time: 08.05
 * To change this template use File | Settings | File Templates.
 */

public interface ICaptchaService {
    void processResponse(final String response) throws ReCaptchaInvalidException;

    String getReCaptchaSite();

    String getReCaptchaSecret();
}
